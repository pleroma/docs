#!/bin/sh

fetch() {
  if [ $LOCAL = false ]; then
    mkdir .tmp
    git clone $FRONTEND_REPO --branch develop --depth 1 .tmp/fe
    git clone $BACKEND_REPO --branch $BRANCH --depth 1 .tmp/be
    FRONTEND_REPO=.tmp/fe
    BACKEND_REPO=.tmp/be
  fi
  rm -rf docs/frontend
  rm -rf docs/backend
  mkdir docs/frontend
  mkdir docs/backend
  cp -r $FRONTEND_REPO/docs/* docs/frontend
  cp -r $BACKEND_REPO/docs/* docs/backend
  rm -rf .tmp
}

build() {
  mkdocs build
}

all() {
	fetch
	build
}

FRONTEND_REMOTE_REPO_DEFAULT='https://git.pleroma.social/pleroma/pleroma-fe.git'
BACKEND_REMOTE_REPO_DEFAULT='https://git.pleroma.social/pleroma/pleroma.git'

FRONTEND_LOCAL_REPO_DEFAULT='../pleroma-fe'
BACKEND_LOCAL_REPO_DEFAULT='../pleroma'
BRANCH='develop'

if [ -z "$1" ] || [ "$1" = "--help" ]; then
	echo "Usage: $(basename "$0") <stage> [<options>]

  The stages are:

  fetch [<options>]
    Fetch frontend and backend documentation and dump it into \`docs\`.
    The location of frontend and backend repositories defaults to
    $FRONTEND_REMOTE_REPO_DEFAULT and $BACKEND_REMOTE_REPO_DEFAULT
    respectively and can be overriden by \`--frontend-repo\` and \`--backend-repo\`.
    The branch defaults to \`$BRANCH\` and can be overriden by \`--branch\`.

    If you want to use local copies of the repositories, add \`--local\`
    to options. Then the location of frontend and backend repositiories
    will default to $FRONTEND_LOCAL_REPO_DEFAULT and $BACKEND_LOCAL_REPO_DEFAULT respectively and
    can be overriden by \`--frontend-repo\` and \`--backend-repo\` as well.

  build [<options>]
   Build the documentation

  all [<options>]
   Execute all stages
  "
else
	LOCAL=false
        stage="$1"
	shift
	while echo "$1" | grep "^-" > /dev/null; do
		case "$1" in
		--local)
			LOCAL=true
			shift
			;;
		--frontend-repo)
			FRONTEND_REPO="$2"
			shift 2
			;;
		--backend-repo)
			BACKEND_REPO="$2"
			shift 2
			;;
		--branch)
			BRANCH="$2"
			shift 2
			;;
		-*)
			echo "invalid option: $1" 1>&2
			shift 1
			;;
		esac
	done
	if [ $LOCAL = true ]; then
	  FRONTEND_REPO="${FRONTEND_REPO:-$FRONTEND_LOCAL_REPO_DEFAULT}"
	  BACKEND_REPO="${BACKEND_REPO:-$BACKEND_LOCAL_REPO_DEFAULT}"
	else
	  FRONTEND_REPO="${FRONTEND_REPO:-$FRONTEND_REMOTE_REPO_DEFAULT}"
	  BACKEND_REPO="${BACKEND_REPO:-$BACKEND_REMOTE_REPO_DEFAULT}"
	fi
	$stage
fi
